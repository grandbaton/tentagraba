﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputLetters
{
//    public InputLetters()
//    {
//        this.imageBuffer = new ArrayList();
//        LoadImages();
//
//    }
//    static private string l = "jJabxylrLRkKd";
    static private string[] l = new string[]
        {
            "q","w","e","r","t","y","u","i",
            "o","p","a","s","d","f","g","h",
            "j","k","l","z","x","c","v","b",
            "n","m", "caps lock", "scroll lock",
            "right shift", "left shift",
            //"right ctrl", "left ctrl",
            "backspace", "delete",
            "return", "pause", "space",
            "up", "down", "right", "left", "insert",
            "home", "end", "page up", "page down",
            "0", "1", "2", "3", "4", "5", "6", "7",
            "8", "9"
        };
    static private string[] editorList = new string[]
        {
            "q","w","e","r","t","y","u","i",
            "o","p","a","s","d","f","g","h",
            "j","k","l","z","x","c","v","b",
            "n","m", "caps lock", "scroll lock",
            "right shift", "left shift",
            "backspace", "delete",
            "return", "pause", "space",
            "up", "down", "right", "left", "insert",
            "home", "end", "page up", "page down",
            "0", "1", "2", "3", "4", "5", "6", "7",
            "8", "9"
        };
    //public ArrayList imageBuffer;

    static public string[] GetList()
    {
        if (Application.isEditor)
            return editorList;
        return l;
    }
    /*
    public void LoadImages()
    {
        string pathImageAssets = Application.dataPath + @"\Textures\UI\Keyboard\";

        foreach (string fileName in System.IO.Directory.GetFiles(pathImageAssets))
        {
            if (fileName.EndsWith(".png"))
            {
                WWW www = new WWW(fileName);
                Texture2D texTmp = new Texture2D(128, 128, TextureFormat.DXT5, false);
                texTmp.name = Path.GetFileNameWithoutExtension(fileName);    
                www.LoadImageIntoTexture(texTmp);
                this.imageBuffer.Add(texTmp);
            }
        }
    }*/
}

public class GameManager : MonoBehaviour {

    public GameObject poulpe;
    public GameObject level;
    public float range = 20f;
    public GameObject gameOverScreen;
    public GameObject winScreen;

    private HashSet<Grab> availableGrabsList;

    private GrabSurface[] surfaces;
    private TentacleManager t;
    private LifeManager lifeManager;
    private Win win;
    private bool won = false;


    private Scene scene;

    // Use this for initialization
    void Start ()
    {
        Cursor.visible = false;
        surfaces = level.GetComponentsInChildren<GrabSurface>();
        availableGrabsList = new HashSet<Grab>();

        t = poulpe.GetComponent<TentacleManager>();
        scene = SceneManager.GetActiveScene();
        lifeManager = poulpe.GetComponent<LifeManager>();
        win = level.GetComponentInChildren<Win>();
	}


	// Update is called once per frame
	void Update ()
    {        
        if (win.levelComplete && !won)
        {
            winScreen.SetActive(true);
            TentacleManager.numberOfTentacle++;
            won = true;
            StartCoroutine(WaitAndReload(3f));
        }

        if (lifeManager.life < 0f)
        {
            gameOverScreen.SetActive(true);
            StartCoroutine(WaitAndReload(3f));
            t.ReleaseTentacles();
                
        }

        if (Input.GetKeyUp(KeyCode.F1))
        {
            ReloadScene();
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
            print("Quit");
        }

	}

    void ReloadScene()
    {
        SceneManager.LoadScene(scene.name);
    }

    IEnumerator WaitAndReload(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        ReloadScene();
    }
}
