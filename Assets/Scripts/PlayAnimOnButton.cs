﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimOnButton : MonoBehaviour {

    private string[] letters;
    public int playerNum;
    private Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        letters = InputLetters.GetList();
	}
	
	// Update is called once per frame
	void Update ()
    {
        foreach (string c in letters)
        {
            if (Input.GetKeyDown(c.ToString()))
//            if (Input.GetAxis(c.ToString()+playerNum.ToString())!=0)
            {
                anim.SetTrigger("play");
                Destroy(this);
            }
        }
	}
}
