﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeManager : MonoBehaviour
{
    private TentacleManager ts;
    public float 
            lavalevel = -10f,
            startLife = 1f,
            depleteSpeed = 0.01f;

    public Material startMaterial, deadMaterial;
    public Collider col;

    public float life;
    private Tentacle[] tentacles;
    private Material mat;
    private ParticleSystem[] smokes;
    private RandomSound[] sizzle;

	// Use this for initialization
	void Start ()
    {
        ts = GetComponent<TentacleManager>();
        tentacles = ts.GetTentacles();
        smokes = new ParticleSystem[tentacles.Length+1];
        sizzle = new RandomSound[tentacles.Length+1];
        life = startLife;
        mat = GetComponentInChildren<MeshRenderer>().material;
        mat.color = startMaterial.color;
        int i = 0;
        foreach (Tentacle t in tentacles)
        {
            foreach (SkinnedMeshRenderer r in t.GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                r.material = mat;
            }
            smokes[i] = t.GetComponentInChildren<ParticleSystem>();
            ParticleSystem.EmissionModule em = smokes[i].emission;
            em.rateOverTimeMultiplier = 0f;

            sizzle[i] = t.GetComponentInChildren<RandomSound>();
            i++;
        }
        smokes[tentacles.Length] = GetComponentInChildren<ParticleSystem>();
        sizzle[tentacles.Length] = GetComponentInChildren<RandomSound>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (col.transform.TransformPoint(Vector3.zero).y < lavalevel + 2.5f)
        {
            life -= depleteSpeed;
            ParticleSystem.EmissionModule em = smokes[tentacles.Length].emission;
            em.rateOverTimeMultiplier = 8f;
            sizzle[tentacles.Length].PlaySound();
        }
        else
        {
            ParticleSystem.EmissionModule em = smokes[tentacles.Length].emission;
            em.rateOverTimeMultiplier = 0f;
            sizzle[tentacles.Length].StopSound();
        }

        int i = 0;
        foreach (Tentacle t in tentacles)
        {
//            Transform tr = t.damageTrigger[0].transform;
//            Transform tr2 = t.damageTrigger[1].transform;

//            if (tr.TransformPoint(Vector3.zero).y < lavalevel || tr.TransformPoint(Vector3.zero).y < lavalevel )
            if (t.isTakingDamage)
            {
                life -= depleteSpeed;
                ParticleSystem.EmissionModule em = smokes[i].emission;
                em.rateOverTimeMultiplier = 8f;

                sizzle[i].PlaySound();
            }
            else
            {
                ParticleSystem.EmissionModule em = smokes[i].emission;
                em.rateOverTimeMultiplier = 0f;
                sizzle[i].StopSound();
            }
            i++;
        }

        mat.Lerp(deadMaterial, startMaterial, life);
	}
}
