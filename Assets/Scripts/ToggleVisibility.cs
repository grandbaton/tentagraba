﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleVisibility : MonoBehaviour {

    public string key = "tab";
    private Text txt;

    const string letters = "qwertyuiopasdfghjklzxcvbnm";

	// Use this for initialization
	void Start ()
    {
        txt = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(key))
        {
            txt.enabled = !txt.enabled;
        }

        if (txt.enabled)
        {
            foreach (char c in letters)
            {
                if (Input.GetKeyDown(c.ToString()))
                {
                    txt.enabled = false;
                }
            }
        }
	}
}
