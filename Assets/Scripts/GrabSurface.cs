﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabSurface : MonoBehaviour {

    public Grab grabPrefab;

    public int numberOfGrab = 10;

    private Grab[] grabArray;
    private Vector3 scale;

    private void SpawnGrabs()
    {
        for (int i = 0; i < numberOfGrab; i++)
        {
            float randomX = Random.Range(-scale.x, scale.x)*5;
            float randomZ = Random.Range(-scale.z, scale.z)*5;
            Grab newGrab = Instantiate(grabPrefab);
            //newGrab.transform.parent = this.transform;
            newGrab.transform.localPosition = new Vector3(randomX, 0, randomZ) + this.transform.position;
            grabArray[i] = newGrab;
        }
    }

    public InOutGrabs GetGrabsInRange( Vector3 position, float range )
    {
        List<Grab> grabs = new List<Grab>();
        List<Grab> outGrabs = new List<Grab>();
        InOutGrabs g = new InOutGrabs();
        foreach (Grab grab in grabArray)
        {
            float dist = Vector3.Magnitude(grab.transform.position - position);
            if (dist < range)
            {
                grabs.Add(grab);
            }
            else if (dist > range + 1f)
            {
                outGrabs.Add(grab);
            }
        }
        g.inGrabs = grabs.ToArray();
        g.outGrabs = outGrabs.ToArray();
        return g;
    }

	// Use this for initialization
	void Awake ()
    {
        grabArray = new Grab[numberOfGrab];
        scale = this.transform.localScale;
        SpawnGrabs();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
