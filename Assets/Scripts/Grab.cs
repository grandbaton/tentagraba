﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grab : MonoBehaviour {
    
    [SerializeField]
    private Color grabbedColor, notGrabbedColor, farColor;
    [SerializeField]
    private float FadeSpeed = 10f;
    [SerializeField]
    private Sprite[] keySprites;
    private Color targetColor;
    private bool _grabbed = false;
    private int playersInRange = 0;
    private string _letter;

    private TextMesh text;
    private Image keyImage;

    public bool grabbed
    {
        get{return _grabbed;}
    }

    public void DisplayLetter(string letter)
    {
        _letter = letter;
        targetColor = notGrabbedColor;
        text.color = Color.clear;
        text.text = letter.ToUpper();//REMOVE Uppercase to workd with pad
        keyImage.color = Color.clear;
        keyImage.sprite = GetSprite();
        playersInRange++;
    }

    public string GetLetter()
    {
        return _letter;
//        return text.text.ToLower();//REMOVE Lowercase to workd with pad
    }

    public void SetSelected()
    {
        targetColor = grabbedColor;
        _grabbed = true;
    }

    public void SetUnselected()
    {
        targetColor = notGrabbedColor;
        _grabbed = false;
    }

    public void SetFar()
    {
        targetColor = farColor;
    }

    public void SetClose()
    {
        targetColor = notGrabbedColor;
    }

    public void StopDisplay()
    {
        playersInRange--;
        if (playersInRange <= 0)
        {
            text.text = "";
//            _letter = "";
            targetColor = Color.clear;
        }
    }

    private Sprite GetSprite()
    {
        foreach (Sprite s in keySprites)
        {
            if (s.name == _letter)
                return s;
        }
        return null;
    }

	// Use this for initialization
	void Awake ()
    {
        text = GetComponentInChildren<TextMesh>();
        keyImage = GetComponentInChildren<Image>();
        text.text = "";
        _letter = "";
        keyImage.color = Color.clear;
	}
	
	// Update is called once per frame
	void Update ()
    {
        text.color = Color.Lerp(text.color, targetColor, Time.deltaTime * FadeSpeed);
        keyImage.color = Color.Lerp(keyImage.color, targetColor, Time.deltaTime * FadeSpeed);
	}
}
