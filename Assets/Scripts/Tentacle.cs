﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tentacle : MonoBehaviour {

    public GameObject target;
    public HingeJoint baseHinge;
    public float tentacleSpeed = 10f;
    [HideInInspector]
    public Grab grab;
    private RandomSound sound;
    public bool isTakingDamage = false;

    public int damageCount = 0;

//    private bool targetIsKinematic = true;
    private Transform targetTransform;
    private Rigidbody targetRB;

    

    public void ReleaseTentacle()
    {
        targetRB.isKinematic = false;
        grab = null;
    }

    public void SetTarget(Grab g)
    {
        
        targetRB.isKinematic = true;
        grab = g;
        sound.PlaySlurp();
        StartCoroutine( BlendToPosition(g.transform.position) );

    }

    IEnumerator BlendToPosition(Vector3 pos)
    {
        while (Vector3.Magnitude(targetTransform.position - pos) > 0.5f && targetRB.isKinematic)
        {
//            targetRB.AddForce((pos - targetTransform.position) * 500f);
//            Debug.DrawRay(targetTransform.position, pos - targetTransform.position);
            targetTransform.position = Vector3.Lerp(targetTransform.position, pos, Time.deltaTime * tentacleSpeed);
            targetTransform.rotation = Quaternion.LookRotation(pos - targetTransform.position);//Quaternion.Lerp(targetTransform.rotation, Quaternion.LookRotation(pos - targetTransform.position, Vector3.down), Time.deltaTime * tentacleSpeed);
            yield return null;
        }
    }

	// Use this for initialization
	void Awake ()
    {
        targetTransform = target.transform;
        targetRB = target.GetComponent<Rigidbody>();
        grab = null;
        sound = GetComponentInChildren<RandomSound>();
	}

	// Update is called once per frame
	void Update ()
    {
        if (damageCount > 0)
        {
            isTakingDamage = true;
        }
        else
        {
            isTakingDamage = false;
        }

	}
}
