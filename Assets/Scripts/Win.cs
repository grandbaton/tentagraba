﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour {

    private SphereCollider trig;
    private MeshRenderer r;
    public bool levelComplete = false;
    private Material mat;
    private Color color;
    private float sin;

    void Awake()
    {
        trig = GetComponent<SphereCollider>();
        r = GetComponent<MeshRenderer>();
        mat = r.material;
        color = mat.color;
    }

    void Update()
    {
        //mat.color = new Color(color.r, color.g, color.b, Mathf.Sin(Time.time*5)*.25f + .5f);
        sin = Mathf.Sin(Time.time*3)*.25f + .25f;
        mat.SetColor("_EmissionColor",new Color(sin, sin , sin));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "PoulpeBody")
        {
            levelComplete = true;
            r.enabled = false;
        }
    }
}
