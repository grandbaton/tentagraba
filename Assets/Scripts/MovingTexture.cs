﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTexture : MonoBehaviour {

    private Material mat;

	// Use this for initialization
	void Start () {
        
        mat = GetComponent<MeshRenderer>().material;
	}
	
	// Update is called once per frame
	void Update ()
    {
        mat.SetTextureOffset("_MainTex", new Vector2(Mathf.Sin(Time.time*.5f)*.05f, Mathf.Sin(Time.time*.2f)*.1f));
	}
}
