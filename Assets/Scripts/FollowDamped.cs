﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowDamped : MonoBehaviour {

    public Transform objectToFollow;
    public float followSpeed = 10f;
    public Vector3 scale;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        this.transform.position = Vector3.Scale(Vector3.Lerp(this.transform.position, objectToFollow.position, Time.deltaTime * followSpeed), scale);
	}
}
