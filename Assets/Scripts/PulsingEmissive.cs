﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulsingEmissive : MonoBehaviour {

    private MeshRenderer r;
    private Material mat;
    private Color color;
    private float sin;

    public float speed = 3f;


    void Awake()
    {
        r = GetComponent<MeshRenderer>();
        mat = r.material;
        speed += Random.Range(-1, 1);
    }

    void Update()
    {
        sin = Mathf.Sin(Time.time*speed)*.25f + .25f;
        mat.SetColor("_EmissionColor",new Color(sin, sin , sin));
    }
}
