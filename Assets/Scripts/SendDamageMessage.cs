﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendDamageMessage : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Damage"))
        {
            GetComponentInParent<Tentacle>().damageCount++;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Damage"))
        {
            GetComponentInParent<Tentacle>().damageCount--;
        }
    }
}
