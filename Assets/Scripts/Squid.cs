﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Squid : MonoBehaviour {

    public int playerNum = 1;
    private SphereCollider insideCollision;

    private List<Grab> unassignedGrabsInrange;
    private HashSet<Grab> assignedGrabs;
    private HashSet<Grab> grabbedGrabs;
    private HashSet<Grab> WaitingToForUngrab;
    private TentacleManager t;
    private List<string> availableLetters;
    private List<string> assignedLetters;
    private int numberOfLetters;


    void Start ()
    {
        Assert.raiseExceptions = true;
        insideCollision = GetComponent<SphereCollider>();
        availableLetters = new List<string>(InputLetters.GetList());
        assignedLetters = new List<string>();
        unassignedGrabsInrange = new List<Grab>();
        assignedGrabs = new HashSet<Grab>();
        grabbedGrabs = new HashSet<Grab>();
        WaitingToForUngrab = new HashSet<Grab>();
        t = GetComponent<TentacleManager>();
        numberOfLetters = availableLetters.Count;
    }

    public void RemoveAssignedGrab( Grab g )
    {
        if (g.grabbed)
        {
            bool added = WaitingToForUngrab.Add(g);
            Debug.Assert(added, "Couldnt add to waitingForUngrab");
        }
        else
        {
            string letter = g.GetLetter();
            Assert.IsTrue(letter != "", "Letter blank");

            if (letter != "")
            {
                Debug.Assert(assignedLetters.Contains(letter), "Letter not found in assigned list");
                bool removed = assignedLetters.Remove(letter);
                Debug.Assert(removed, "Letter not removed");
                availableLetters.Add(letter);
                Debug.Assert(assignedLetters.Count + availableLetters.Count == numberOfLetters, "Letter number discrepancy");

                g.StopDisplay();
                Debug.Assert(assignedGrabs.Contains(g), "Assigned grab does't Contains");
                bool removeSuccess = assignedGrabs.Remove(g);
                Debug.Assert(removeSuccess, "Didnt remove");
                if (!removeSuccess)
                {
                    unassignedGrabsInrange.Remove(g);
                }
            }
            else
                print("BORDEL!");
        }
    }

    void AddAssignedGrab( Grab g )
    {
        if (availableLetters.Count > 0)
        {
            int randomIndex = Random.Range(0, availableLetters.Count);
            string randomLetter = availableLetters[randomIndex];

            g.DisplayLetter(randomLetter.ToString());
            unassignedGrabsInrange.Remove(g);
            assignedGrabs.Add(g);

            assignedLetters.Add(randomLetter);
            availableLetters.RemoveAt(randomIndex);
        }
    }

    void Update ()
    {
        if (unassignedGrabsInrange.Count > 0 && availableLetters.Count > 0)
        {
            AddAssignedGrab(unassignedGrabsInrange[0]);
        }

        string player = playerNum.ToString();


        foreach (Grab g in assignedGrabs)
        {
            if (!g.grabbed)
            {
                string letter = g.GetLetter();

                if (Input.GetKeyDown(letter))
                {
                    t.GrabOn(g);
                    grabbedGrabs.Add(g);
                }
            }
        }

        //Check if player released a button assigned to a grabbed grab
        foreach ( Grab g in grabbedGrabs)
        {
            string letter = g.GetLetter();
            if (letter == "")
            {
                RemoveAssignedGrab(g);
                continue;
            }
            if (!Input.GetKey(letter))
                //            if (Input.GetAxis(letter+player)==0)
            {
                t.UnGrab(g);
                grabbedGrabs.Remove(g);
                if(WaitingToForUngrab.Contains(g))
                {
                    RemoveAssignedGrab(g);
                    WaitingToForUngrab.Remove(g);
                }
                break;
            }
        }
        if (Input.GetKey("right ctrl"))
        {
            print("ctrl");
        }

    }

    void AddGrabInRange(Grab g)
    {
        if (!g.grabbed)
        {
            if (!unassignedGrabsInrange.Contains(g) && !assignedGrabs.Contains(g))
            {
                unassignedGrabsInrange.Add(g);
            }
            else
            {
                g.SetClose();
            }
        }
    }





    void OnTriggerEnter(Collider other)
    {
        Grab g = other.gameObject.GetComponent<Grab>();
        if (g)
        {
            AddGrabInRange(g);
        }
    }

    void OnTriggerExit(Collider other)
    {
        Grab g = other.gameObject.GetComponent<Grab>();
        if (g && !g.grabbed && assignedGrabs.Contains(g))
        {
            g.SetFar();
//            RemoveAssignedGrab(other.gameObject.GetComponent<Grab>());
        }
    }

    //    void DisplayLettersInRange()
    //    {
    //        foreach (GrabSurface surface in surfaces)
    //        {
    //            InOutGrabs grabs = surface.GetGrabsInRange(poulpe.transform.position, range );
    //            foreach (Grab g in grabs.inGrabs)
    //            {
    //                if (!grabbedGrabs.Contains(g))
    //                {
    //                    //if new grab in range
    //                    if (availableGrabsList.Add(g) && )
    //                    {
    //                        //find new Unique letter
    //                        string l;
    //                        //                        int i = 0;
    //                        do
    //                        {
    //                            l = InputLetters.GetList()[Random.Range(0, InputLetters.GetList().Length)].ToString();
    //                            //                            i++;
    //                            //                            if (i > 200)
    //                            //                            {
    //                            //                                print("All letters assigned!");
    //                            //                                break;
    //                            //                            }
    //                        } while (assignedLetters.Add(l) == false);
    //
    //                        g.DisplayLetter(l);//.ToUpper());
    //
    //                    }
    //                }
    //                //                if (fadingList.Contains(g))
    //                //                {
    //                //                    g.StopFade();
    //                //                    fadingList.Remove(g);
    //                //                }
    //            }
    //            foreach (Grab g in grabs.outGrabs)
    //            {
    //                if (!grabbedGrabs.Contains(g) && availableGrabsList.Contains(g))
    //                {
    //                    //                    StartCoroutine(FadeLetter(g));
    //                    //                    fadingList.Add(g);
    //                    availableGrabsList.Remove(g);
    //                    assignedLetters.Remove(g.GetLetter());
    //                    g.StopDisplay();
    //                }
    //            }
    //        }
    //    }
    //
    //    IEnumerator FadeLetter(Grab g)
    //    {
    //        g.Fade();
    //        if (g.stopFadeFlag || grabbedGrabs.Contains(g))
    //        {
    //            g.stopFadeFlag = false;
    //            yield break;
    //        }
    //        yield return new WaitForSeconds(2);
    //        fadingList.Remove(g);
    //        availableGrabsList.Remove(g);
    //        assignedLetters.Remove(g.GetLetter());
    //        g.StopDisplay();
    //    }

}
