﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendExitCollisionMessage : MonoBehaviour {

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Grab"))
        {
//            print("exitOuter " + other.gameObject.GetComponent<Grab>().GetLetter());
            GetComponentInParent<Squid>().RemoveAssignedGrab(other.gameObject.GetComponent<Grab>());
        }
    }
}
