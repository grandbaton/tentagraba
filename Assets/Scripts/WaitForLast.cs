﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WaitForLast : MonoBehaviour {

    [SerializeField]
    private Transform[] objectsToFollow;
    [SerializeField]
    private bool waitBigger = false;

    private enum Axis {X, Y, Z};
    [SerializeField]
    private Axis waitOnAxis = Axis.X;

    private int waitAxis;
    private int objectCount;

	void Start ()
    {
        waitAxis = (int)waitOnAxis;
        objectCount = objectsToFollow.Length;
	}
	
	void FixedUpdate ()
    {
        Vector3 average = Vector3.zero;
        float[] waitAxisValues = new float[objectCount];

        for (int i = 0; i<objectCount; i++)
        {
            Vector3 pos = objectsToFollow[i].position;
            average += pos;
            waitAxisValues[i] = pos[waitAxis];
        }
        average /= objectCount;

        if(waitBigger)
            average[waitAxis] = waitAxisValues.Max();
        else
            average[waitAxis] = waitAxisValues.Min();
        this.transform.position = average;
	}
}