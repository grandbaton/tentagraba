﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleVisibility : MonoBehaviour {

    private Text txt;

    const string letters = "qwertyuiopasdfghjklzxcvbnm";
//    const string letters = "jJabxylrLRkKd";

	// Use this for initialization
	void Start ()
    {
        txt = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (txt.enabled)
        {
            foreach (char c in letters)
            {
                if (Input.GetKeyDown(c.ToString()))
//                if (Input.GetAxis(c.ToString()+"1")!=0)
                {
                    txt.enabled = false;
                    Destroy(this);
                }
            }
        }
	}
}
