using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct InOutGrabs
	{
		public Grab[] inGrabs;
		public Grab[] outGrabs;
	}
