﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSound : MonoBehaviour {

    public AudioClip[] clips;

    public AudioClip[] slurpClip;
    public AudioClip boil;

    private AudioSource a;
    private bool sizzled = false;

    void Start()
    {
        a = GetComponent<AudioSource>();
    }

    public void PlaySound()
    {
        if (!a.isPlaying && !sizzled)
        {
            a.clip = clips[Random.Range(0, clips.Length)];
            a.Play();
            sizzled = true;
        }
        if (!a.isPlaying && sizzled)
        {
            a.clip = boil;
            a.Play();
        }
    }

    public void PlaySlurp()
    {
        a.clip = slurpClip[Random.Range(0, slurpClip.Length)];
        a.Play();

    }

    public void StopSound()
    {
        if (a.isPlaying && sizzled)
        {
            StartCoroutine (AudioFadeOut.FadeOut (a, 0.25f));
        }

        sizzled = false;
    }

}
