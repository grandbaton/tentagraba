﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleManager : MonoBehaviour {

    public Tentacle tentaclePrefab;

    public static int numberOfTentacle = 4;
    public float startAngle = 45f;
    public bool startFree = true;


    private float radialAngle;
    private Rigidbody rb;
    private Tentacle[] tentacleArray;
    private Stack<Tentacle> freeTentacles;


//    private TentacleCount tentacleCounter;

    public void GrabOn(Grab g)
    {
        if (freeTentacles.Count > 0)
        {
            Tentacle t = freeTentacles.Pop();//.Dequeue();
            t.SetTarget(g);
            g.SetSelected();
        }
    }

    public void UnGrab(Grab g)
    {
        Tentacle tentacle = null;
        foreach (Tentacle t in tentacleArray)
        {
            if (t.grab == g)
            {
                tentacle = t;
            }
        }
        if (tentacle != null)
        {
            freeTentacles.Push(tentacle);//.Enqueue(tentacle);
            tentacle.ReleaseTentacle();
            g.SetUnselected();
        }
    }

    public void ReleaseTentacles()
    {
        foreach (Tentacle ten in tentacleArray)
        {
            ten.ReleaseTentacle();
        }
    }

    void SpawnTentacles()
    {
        radialAngle = 360f/numberOfTentacle;
        for (int i = 0; i < numberOfTentacle; i++)
        {
            Tentacle newTentacle = Instantiate<Tentacle>(tentaclePrefab, transform.position, Quaternion.Euler(90, radialAngle*i, startAngle));
            newTentacle.baseHinge.connectedBody = rb;
            tentacleArray[i] = newTentacle;

            if (startFree) newTentacle.ReleaseTentacle();
        }
    }

	// Use this for initialization
	void Awake ()
    {
//        tentacleCounter = FindObjectOfType<TentacleCount>();

        tentacleArray = new Tentacle[numberOfTentacle];
        rb = GetComponentInChildren<Rigidbody>();
        SpawnTentacles();
        freeTentacles = new Stack<Tentacle>(tentacleArray);
	}
	
    public Tentacle[] GetTentacles()
    {
        return tentacleArray;
    }

	// Update is called once per frame
//	void Update ()
//    {
//	}
}
